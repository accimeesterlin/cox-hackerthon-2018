
const axios = require('axios');
const router = require('express').Router();
const { google } = require('googleapis');

const mongojs = require('mongojs')
const db = mongojs('mydb', ['leads']);

const oauth2Client = new google.auth.OAuth2(
    '55846227385-7k6s4ob7lmsf3meksf1kfkghr046a0l9.apps.googleusercontent.com',
    'Hy606OWiS250tjsE5nqK-VU9',
    'http://localhost:9002/google/token'
);

const scopes = [
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/calendar',
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/userinfo.email '
];


router.get('/auth', (req, res) => {
    const url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes
    });

    res.json({
        url
    });

});




router.get('/token', async (req, res) => {
    const code = req.query.code;
    const { tokens } = await oauth2Client.getToken(code)
    oauth2Client.setCredentials(tokens);


    db.leads.save({ ...tokens }, (err, data) => {
        if (err) {
            res.json({
                error: 'error occurs'
            });
        }
        res.redirect(`http://localhost:3000?id=${data._id}`);
    });
});


router.get('/profile/:id', async (req, res) => {
    const id = req.params.id

    try {
        db.leads.findOne({
            _id: mongojs.ObjectId(id)
        }, async (err, user) => {
            if (err) {
                res.json({
                    error: 'Sorry something happened'
                });
            }
            const url = 'https://www.googleapis.com/plus/v1/people/me';
            const { data } = await axios({
                url,
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${user.access_token}`
                }
            });
    
            delete data.access_token
            delete data.id_token
            delete data.expiry_date
            delete data.scope
            delete data.token_type
    
    
            res.json({
                ...data
            });
    
        });
    } catch (error) {
        res.json({
            error: 'Internal error'
        });
    }
});

// oauth2Client.on('tokens', (tokens) => {
//     if (tokens.refresh_token) {
//         // store the refresh_token in my database!
//         console.log(tokens.refresh_token);
//     }
//     console.log(tokens.access_token);
// });


module.exports = router;