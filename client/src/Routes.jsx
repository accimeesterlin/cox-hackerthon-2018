import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import {
    Calendar,
    Leads,
    Profile
} from './components';

export default class MainRoute extends Component {


    render() {

        // JSX
        return(
            <div>
                <Route exact path = '/' component={Profile}/>
                <Route path = '/leads' component={Leads}/>
                <Route path = '/calendar' component={Calendar}/>
            </div>
        );
    }
};

