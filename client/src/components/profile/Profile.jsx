import React, { Component } from 'react';
import qs from 'stringquery';
import axios from 'axios';


export default class App extends Component {

  state = {
    isAuthenticated: false,
    leads: 2
  };

  componentDidMount = () => {
    if (this.props.location.search) {
      const data = qs(this.props.location.search);
      this.setState({ isAuthenticated: true });

      axios({
        url: `/google/profile/${data.id}`,
        method: 'GET'
      })
        .then((response) => {
          const data = response.data;
          this.setState({ ...data });
        })
        .catch((error) => {
          this.setState({ isAuthenticated: false });
        })
    }
  };


  googleAuthentication = () => {
    axios({
      url: '/google/auth',
      method: 'GET'
    })
      .then((response) => {
        window.location = response.data.url;
      })
      .catch((error) => {
        console.log('Error: ', error);
      });
  };

  displayUserInfo = () => {
    if (this.state.isAuthenticated && this.state.displayName) {
      return (
        <div>
          <h2>Name: {this.state.displayName}</h2>
          <p>Leads Submitted: {this.state.leads}</p>
        </div>
      );
    }
    
    return null;
  };


  render() {
    console.log('ID: ', this.state);
    // JSX
    return (
      <div>
        <h2>I am the Profile Component</h2>
        {this.state.isAuthenticated && this.state.displayName ? <p>User is authenticated</p> : <button onClick={this.googleAuthentication}>Google Authenticate</button>}

        {this.displayUserInfo()}
      </div>
    );
  }
}